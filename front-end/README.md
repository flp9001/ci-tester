# mercuryo

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Required software:
* [Node.JS](http://nodejs.org/) v6.3.0 or higher.
* [Ruby](http://rubyinstaller.org/) v2.0 or higher.

Install Compass:
    
    gem install compass

Install Node.JS dependencies:

    npm install -g grunt-cli bower yo generator-karma generator-angular

You may need to use 'sudo' to install with `npm -g`.


### Building

Whenever dependencies change (bower or npm), install with:
    
    cd front-end/
    npm install
    bower install

Then run `grunt build` to build the front-end into Django.
Or, run `grunt keep-building` to continuosly build and reload the page on change.


### Testing

Running `grunt test` will run the unit tests with karma.
