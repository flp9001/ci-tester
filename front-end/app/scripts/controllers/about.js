'use strict';

/**
 * @ngdoc function
 * @name mercuryoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the mercuryoApp
 */
angular.module('mercuryoApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
