'use strict';

/**
 * @ngdoc function
 * @name mercuryoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mercuryoApp
 */
angular.module('mercuryoApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
